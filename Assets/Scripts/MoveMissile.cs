﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveMissile : MonoBehaviour
{
    float speed = 2.0f;

    // Start is called before the first frame update
    void Start()
    {
        speed = Random.Range(4.0f, 8.0f);
    }

    // Update is called once per frame
    void Update()
    {
        // Move the object forward along its z axis 1 unit/second.
        transform.Translate(Vector3.forward * Time.deltaTime * speed);

        // Move the object downward in world space 1 unit/second.
        transform.Translate(Vector3.down * Time.deltaTime * speed, Space.World);
    }

  
}
