﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour
{
    public int score = 0;
    public int lives = 3;

    public Text textScore;
    public Text textLives;

    public static GameLogic instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        textScore.text = "Score: " + score.ToString();
        textLives.text = "Health: " + lives.ToString();
        if (lives <= 0)
        {
            endGame();
        }
    }

    void endGame()
    {
        SceneManager.LoadScene("reset");
    }
}
