﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndScore : MonoBehaviour
{
    public Text finalDesc;
    // Start is called before the first frame update
    void Start()
    {
        finalDesc.text = "The poor village was destroyed :(" + " - Current Score: " + GameLogic.instance.score.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
