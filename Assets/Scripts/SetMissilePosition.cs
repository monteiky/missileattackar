﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetMissilePosition : MonoBehaviour
{
    // Reference to the Prefab. Drag a Prefab into this field in the Inspector.
    public GameObject myPrefab;
    // Start is called before the first frame update
    void Start()
    {
        myPrefab.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        CancelInvoke("SpawnMissile");
        InvokeRepeating("SpawnMissile", 5.0f, 5.0f);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void SpawnMissile()
    {
        float numX = UnityEngine.Random.Range(-35.0f, 40.0f);
        float numZ = UnityEngine.Random.Range(60.0f, 100.0f);
        Vector3 position = new Vector3(numX, 120.0f, numZ);
        // This script will simply instantiate the Prefab when the game starts.
        Instantiate(myPrefab, position, Quaternion.Euler(90, 60, 0));
    }
}
