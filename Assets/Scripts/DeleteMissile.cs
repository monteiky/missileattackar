﻿using Microsoft.MixedReality.Toolkit.Input;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteMissile : MonoBehaviour, IMixedRealityInputHandler, IMixedRealityPointerHandler
{
    // Start is called before the first frame update
    void Start()
    {
    }

    void IMixedRealityInputHandler.OnInputDown(InputEventData eventData) // successfully clicked missile
    {

    }

    public void OnPointerUp(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerDown(MixedRealityPointerEventData eventData)
    {
        print("deleting missile");
        Transform.Destroy(gameObject);
        GameLogic.instance.score += 10000;
    }

    public void OnPointerClicked(MixedRealityPointerEventData eventData)
    {
    }

    public void OnPointerDragged(MixedRealityPointerEventData eventData) { }

    public void OnInputUp(InputEventData eventData)
    {
        throw new System.NotImplementedException();
    }
}
