﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileGazeHandler : MonoBehaviour
{
    Color ogColor;
    // Start is called before the first frame update
    void Start()
    {
        ogColor = gameObject.GetComponent<MeshRenderer>().material.color;
    }

    public void OnFocusEnter()
    {
        gameObject.GetComponent<MeshRenderer>().material.color = Color.red;
    }

    public void OnFocusExit()
    {
        gameObject.GetComponent<MeshRenderer>().material.color = ogColor;
    }
}
