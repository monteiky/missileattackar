﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteMissileOnLanding : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject i in GameObject.FindGameObjectsWithTag("Missile"))
        {
            if (i.transform.position.y <= 2.0)
            {
                Destroy(i);
                GameLogic.instance.lives -= 1;
            }
        }

    }
}
